﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    GameController m_gc;

    private void Start()
    {
        //FindObjectOfType: tìm đế các kiểu
        //tham chiếu m_gc 
        m_gc = FindObjectOfType<GameController>();
    }

    //phương thức bắt va chạm
    private void OnCollisionEnter2D(Collision2D col)
    {
        //nếu mà cái vùng va cham(col) va cham với Tag Player là tag của Square
        if (col.gameObject.CompareTag("Player"))
        {
            //tăng số lần hứng trúng bóng
            m_gc.IncrementScore();

            Destroy(gameObject);//hàm huy đối tượng

            Debug.Log("Đã hứng trúng quả bóng");
        }
    }
    //phuong thức va chạm đi xuyên qua
    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Deathzone"))
        {
            //trò chơi kết thúc
            m_gc.SetGameOver(true);

            Destroy(gameObject);//hàm huy đối tượng
            Debug.Log("Đã Hứng trượt quả bóng Game Over");
        }
    }
}
