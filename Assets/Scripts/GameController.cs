﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public GameObject ball;
    public float spawnTime;//thời gian tạo quả bóng tiếp theo
    float m_spawnTime;//biến lưu giá trị của spawnTime
    
    // int m_score = private int m_score;
    int m_score; 
    bool m_isgameOver;

    UI m_ui;

    // Start is called before the first frame update
    void Start()
    {
        m_spawnTime = 0;
        //tham chiếu đến UI
        m_ui = FindObjectOfType<UI>();
        m_ui.SetScoreText("Score: " + m_score);
       
    }

    // Update is called once per frame
    void Update()
    {
        // giảm thời gian tạo ra quả bóng
        m_spawnTime -= Time.deltaTime;

        //ktra trạng thái gameover
        if (m_isgameOver)
        {
            m_spawnTime = 0;
            //show cái giao diện gameover cho ng dùng
            m_ui.ShowGameOverPanel(true);
            return;//ngắt tắt cả dòng code bên dưới nó 
        }

        if(m_spawnTime <= 0)//nếu mà m_spawnTime <=0 thì gọi hàm SpawnBall
        {
            SpawnBall();
            //trả spawnTime về giá trị ban đầu
            m_spawnTime = spawnTime;
        }
    }
    //phương thức tạo ra quả bóng
    public void SpawnBall()
    {
        //tạo ra quả bóng ngẫu nhiên trong khoảng từ -7 ->7 khoảng cách lên camera là 6
        Vector2 spawnPos = new Vector2(Random.Range(-7, 7), 6);

        if (ball)
        {
            //tạo đối tượng trên Scence
            //Instantiate(đối tượng, vị trí, góc quay)
            Instantiate(ball, spawnPos, Quaternion.identity);
        }
    }
    //nút replay
    public void RePlay()
    {
        m_score = 0;
        m_isgameOver = false;
        m_ui.SetScoreText("Score: " + m_score);
        m_ui.ShowGameOverPanel(false);
    }

    public void SetScore(int value)
    {
        m_score = value;
    }
    public int GetScore()
    {
        return m_score;
    }
    //phương thức tăng điêm khi hứng được bóng
    public void IncrementScore()
    {
        m_score++;

        m_ui.SetScoreText("Score: " + m_score);
    }

    public void SetGameOver(bool value)
    {
        m_isgameOver = value;
    }
    public bool GetGameOver()
    {
        return m_isgameOver;
    }
}
