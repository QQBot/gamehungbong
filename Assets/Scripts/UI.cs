﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    public Text scoreText;

    public GameObject gameoverPanel;

    public Image imageGame;

    public void SetScoreText(string txt)
    {
        if (scoreText)
        {
            scoreText.text = txt;
        }
    }

    //phương thức hiện hay ẩn đối tượng game trên màng hình người chơi
    public void ShowGameOverPanel( bool isshow)
    {
        if (gameoverPanel)
        {
            gameoverPanel.SetActive(isshow);
        }
    }

    
}
