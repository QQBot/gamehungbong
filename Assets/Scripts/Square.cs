﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Square : MonoBehaviour
{
    public float moveSpeed;

    float xDirection;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //khi bấm nút sang trái trả về -1, sang phải là 1
        xDirection = Input.GetAxisRaw("Horizontal");
        
        float moveStep = moveSpeed * xDirection * Time.deltaTime;

        //nếu mà giá đỡ lớn = -8 và nhỏ hơn = 8 thì hk di chuyển nữa // xDirection < 0 hoăc > 0 là nút trái phải
        if ((transform.position.x <= -8f && xDirection < 0)|| (transform.position.x >= 8f && xDirection > 0))
        {
            return;
        }
       
        transform.position = transform.position + new Vector3(moveStep, 0, 0);
        

    }
}
